# Office Queue

## Install

```sh
yarn install
```

## Usage

### Start server

```sh
yarn run start
```

### Format Code

```sh
yarn run format
```

## Run tests

```sh
yarn run test
```
